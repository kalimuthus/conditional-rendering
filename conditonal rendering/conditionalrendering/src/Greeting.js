import React from 'react';
import Usergreeting from './Usergreeting';
import Guestgreeting from './Guestgreeting';
import Conditional from './Conditional'
function Greeting(props) {
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn) {
      return <Usergreeting />;
    }
    return <Guestgreeting />;
  }
export default Greeting;